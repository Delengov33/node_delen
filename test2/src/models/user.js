const mongoose = require('mongoose');
var validator = require('validator');

const User = mongoose.model("User", {
    name: {type: String, required: true, trim: true},
    age: {
        type: Number,
        value: 0,
        validate(value) {
            if (value <= 0) {
                throw new Error("Age must be a positive numver");
            }
        }
    },
    email: {
        type: String,
        toLowerCase: true,
        unique:true,
        validate(value) {
            if (!validator.isEmail(value)) {
                throw new Error("Emaill must be a positive emaill");
            }
        }
    },
    password:{
        type:String,
        required: true,

        trim:true,
        unique:true,
        validate(value){
            if(value.length<7){
                throw new Error("Password must length more seven")
            }else if(value.includes("password")){
                throw new Error("Password dont must world password")
            }

        }

    }

});
module.exports=User;

