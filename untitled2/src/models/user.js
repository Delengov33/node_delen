const mongoose = require('mongoose');
const validator = require('validator');
const bcrypt=require('bcrypt');
const jwt=require('jsonwebtoken');




//const User = mongoose.model("User", );

let UserSchema=new mongoose.Schema({
    name: {type: String, required: true, trim: true},
    age: {
        type: Number,
        value: 0,
        validate(value) {
            if (value <= 0) {
                throw new Error("Age must be a positive numver");
            }
        }
    },
    email: {
        type: String,
        toLowerCase: true,
        unique:true,
        validate(value) {
            if (!validator.isEmail(value)) {
                throw new Error("Emaill must be a positive emaill");
            }
        }
    },
    password:{
        type:String,
        required: true,

        trim:true,
        unique:true,
        validate(value){
            if(value.length<7){
                throw new Error("Password must length more seven")
            }else if(value.includes("password")){
                throw new Error("Password dont must world password")
            }

        }

    },
    tokens:[{
        token:{
            type:String,
            required:true
        }
    }]

});




UserSchema.pre('save',async function (next){
   const user =this;
   if(user.isModified('password')){
       user.password=await  bcrypt.hash(user.password,8);
   }
   next();
});

UserSchema.statics.findOneByCredentials=async (email,password)=>{
    const user=await User.findOne({email});
    if(!user){
        throw new Error("User not found");
    }
    const isMatch=await bcrypt.compare(password,user.password);
    if(!isMatch){
        throw new Error('Passwords don`t match');
    }
    return user;
}
UserSchema.methods.generateAuthToken=async function(){
    const user=this;
    const token=jwt.sign({_id:user._id.toString()},'helloWorld');
    user.tokens=user.tokens.concat({token});
    await user.save()
    return token;
}

const User=mongoose.model('User',UserSchema);

module.exports=User;

