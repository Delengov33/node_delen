const jwt=require("jsonwebtoken");
const User=require("./../models/user");

const express = require("express");
const app = express();
const jsonParser = express.json();

const auth=async (req,res,next)=>{
    try{
        app.use(userRouter);
       const token=req.header("Authorization").replace("Bearer ","");

        const decoded= jwt.verify(token,'helloWorld');
        const user =await User.findOne(({_id:decoded._id,'tokens.token':token}));
        if(!user){
            throw new Error("You dont authenticate");
        }
        req.user=user;
        req.token=token;

     console.log(token);
        next();
        // const decoded=jwt.verify(token,)

    }
    catch (error){
      res.status(403).send(error.message);
    }


}
module.exports=auth;