const express = require("express");
require('./src/bd/mongoose');
const User = require('./src/models/user');
const Task = require('./src/models/task');
const auth=require('./src/middleware/auth');

const app = express();
const jsonParser = express.json();

//маршрутизація
const userRouter=require('./src/routers/user');
app.use('/users',userRouter);

app.use(express.json());

app.use(express.static(__dirname + "/public"));
/*
app.get("/users",auth, (req, res) => {
    const token=req.header("Authorization");
    console.log(token);
    User.find({}).then((users) => {
        res.status(200).send(users);
    }).catch((error) => {
        res.status(500).send();
    });
});


app.get('/users/me',auth,async (req,res)=>{
    res.send(req.user);
})

app.get("/users/deleteAll", (req, res) => {
    User.deleteMany({}).then((users) => {
        res.status(200).send();
    }).catch((error) => {
        res.status(500).send();
    });
});


app.get("/task", (req, res) => {
    Task.find({}).then((users) => {
        res.status(200).send(users);
    }).catch((error) => {
        res.status(500).send();
    });
});


app.get("/task/:id", (req, res) => {
    const id = req.params.id;
    Task.findOne({_id: id}, function (err, user) {
        if (err) return console.log(err);
        res.send(user);
    });

});

app.post("/task", jsonParser, function (req, res) {
    if (!req.body) return res.sendStatus(400);
    const task = {
        description: "tomi3",
        completed: true,

    };


    Task.create(task, function (err, result) {
        if (err) return console.log(err);
        console.log("jlkjlkjlkj");
        res.send(task);

    })
})


app.put("/users/:id", async (req, res) => {

    const user = await User.findById(req.params.id);
    const updates = ["name", "age", "email", "password"];
    console.log("1!!!!!!!!!!!!!! " + user);
    console.log(updates);
    console.log("2!!!!!!!!!!!!!! ");

    console.log("3!!!!!!!!!!!!!! ");
    updates.forEach((update) => user[update] = req.body[update]);
    await user.save().then((users) => {
        res.status(200).send(users);
    }).catch((error) => {
        res.status(500).code();
    })


});

app.get("/users/:id", auth,(req, res) => {
    const id = req.params.id;
    User.findOne({_id: id}, function (err, user) {
        if (err) return console.log(err);
        res.send(user);
    });

});

*/

app.post("/users", jsonParser, function (req, res) {
    if (!req.body) return res.sendStatus(400);
    /* const  name=req.body.name;
     const age=req.body.age;
     const email=req.body.email;
     const password=req.body.password;*/
    // const user = {
    //     name: name,
    //     age: age,
    //     email: email,
    //     password: password
    // };
//?name=Jonathan+Doe&age=23&email=adsklfjsdf@mail.ru&password=erwrwdsffd123
    const user = {
        name: req.body.name,
        age: req.body.age,
        email: req.body.email,
        password: req.body.password
    };
    // User.insert(user,(err,result)=>{
//     if(err){
//         res.send({err});
//     }
//     else{
//         res.send(result.ops[0]);
//     }
// })
    User.create(user, function (err, result) {
        if (err) return console.log(err);
        console.log("jlkjlkjlkj");
        res.send(user);

    })
})

app.post("/users/login", async (req, res) => {

    try {
        console.log("user=  " + req.body.email);
        const user = await User.findOneByCredentials(req.body.email, req.body.password);
        console.log("user=  " + user);
        const token = await user.generateAuthToken();
        res.send({user, token});
    } catch (error) {

        res.status(400).send(error.message);
    }
})
app.post("/users/logout",auth,async(req,res)=>{
    try{
        req.user.tokens=req.user.tokens.filter((item)=>{
            return item.token!=req.token;
        })
      const user=  await req.user.save()
        res.status(202).send(user);
    }catch (e){
        res.status(500).send();
    }
})

app.post("/users/logoutALL",auth,async(req,res)=>{
    try{
        req.user.tokens=[];
        const user=  await req.user.save()
        res.status(202).send(user);
    }catch (e){
        res.status(500).send();
    }
})

app.get("/users/create", (req, res) => {

    User.find({}).then((users) => {
        res.status(200).send(users);
    }).catch((error) => {
        res.status(500).send();
    });
});


const user = new User({
    name: "tomi",
    age: 50,
    email: "tojhkbi@mail.com",
    password: "123456789"
});
const task = new Task({
    description: "Roomkjltoor",
    completed: true
});

task.save().then(() => {
    console.log(task);
}).catch((error) => {
    console.log(error)
});
/*
user.save().then(() => {
   console.log(user);
}).catch((error) => {
   console.log(error)
});*/
app.listen(9000);